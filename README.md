# XnatDataClient: the XNAT Data Client
### Copyright (C) 2019, Washington University School of Medicine

The XNAT Data Client supports data transfer operations to and from the XNAT
server. Check for the latest release version of the XNAT Data Client on [the
XNAT Data Client download page](https://bitbucket.org/xnatdev/data-client/downloads)
or you can access the latest source code from the [XDC git repo on Bitbucket](https://bitbucket.org/xnatdev/data-client):

```
git clone git@bitbucket.org:xnatdev/data-client.git
```

Options for XDC include (the latest version of this help is always available by running XDC with the **-h** option):

     -ls, --listUris    Directs the client to render results as a list of files to
                        the application output. This can be used for such
                        applications as piping output from a call to the server to
                        other commands or processing servers.
     -ts, --trustStore  Indicates the location of the trust store.
     -C, --showStatusLine
                        Tells XDC to show the resulting HTTP status line on exit.
     -H, --header       Indicates the HTTP method to be used for the operation. This
                        can be one of GET, PUT, POST, or DELETE.
     -bs, --bufferSize  Sets the buffer size option. Defaults to 256.
     -auu, --allowUnsmoothUrls
                        Indicates that XDC should allow "unsmooth" URLs. "Unsmooth"
                        URLs may have contiguous path separators (i.e. forward
                        slashes) and other artifacts from string arithmetic and
                        operations. By default, XDC smooths URLs before calling them
                        (i.e. allow unsmooth URLs is false).
     -osa, --outputStreamAdapter
                        Specifies an output stream adapter implementation to handle
                        redirecting the output from your application.
     -db, --data-binary Indicates data to be POSTed with no extra processing. If the
                        data starts with an @, the rest of the data should be a file
                        name from which to read data; carriage returns and newlines
                        are preserved.
     -da, --data-ascii  See -d, --data.
     -d, --data         Indicates data to be POSTed. If the data starts with an @,
                        the rest of the data should be a file name from which to
                        read data; carriage returns and newlines will be stripped
                        out. This is identical to -da, --data-ascii.
     -b, --batch        Indicates that this is a batch operation. Batch operations
                        are currently only supported for download transfers, i.e.
                        GET calls, that retrieve JSON that contains URI or
                        absolutePath references.
     -c, --showStatusCode
                        Tells XDC to show the resulting HTTP status code on exit.
     -a, --useAbsolutePath
                        Indicates that XDC should try to use the absolute path of
                        specified resources for copy operations rather than REST
                        calls.
     -tsPass, --trustStorePassword
                        Provides the password for accessing the trust store.
     -o, --outputName   Indicates the requested output name.
     -l, --local        Indicates the local file to be uploaded.
     -m, --method       Indicates the HTTP method to be used for the operation. This
                        can be one of GET, PUT, POST, or DELETE.
     -k, --useSymlinks  Indicates that files should be linked via symlinks rather
                        that copied during batch operations. Note that this function
                        is dependent on your platform supporting symlinks and the ln
                        command being on your path.
     -h, --help         Displays this help text.
     -xx, --overwrite   Indicates whether the specified output file should be
                        overwritten without prompting if it exists.
     -v, --version      Displays the version of this application.
     -u, --username     The user name for authentication.
     -s, --sessionId    Indicates the session ID to be used for transactions. This
                        replaces username/password authentication options.
     -r, --remote       Indicates the remote resource location. This should be a
                        properly formatted URL, although it can indicate file as
                        well as http resources.
     -p, --password     The password for authentication.
     -pp, --pathPrefix  Indicates a substitution for the path prefix. This lets you
                        replace the first part of the returned absolute path with
                        another path. The path tokens should be separated by the
                        pound sign ('#'). For example, if you know that the absolute
                        path returned by the server will be
                        /data/project/archive/..., but your system has the same data
                        archive mounted at /mnt/data/archive, you would specify the
                        value for this option as /data/project#/mnt/data. Specifying
                        this option implies the absolutePath option set to true.
     -x, --proxy        Indicates the server address for the proxy (if required).

## Building
```
./gradlew jar
java -jar build/install/data-client-shadow/lib/XnatDataClient-1.7.6-all.jar
```
