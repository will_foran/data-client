/*
 * data-client: org.nrg.xnat.client.data.XnatDataClient
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.client.data;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.*;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.*;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.nrg.framework.net.HttpMethod;
import org.nrg.framework.pinto.PintoApplication;
import org.nrg.framework.pinto.PintoException;
import org.nrg.framework.pinto.PintoExceptionType;
import org.nrg.framework.utilities.Reflection;

import javax.activation.MimetypesFileTypeMap;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@SuppressWarnings("UnstableApiUsage")
@PintoApplication(value = "XDC: the XNAT Data Client",
                  version = "property:version",
                  copyright = "(c) 2016 Washington University School of Medicine",
                  introduction = "The XNAT Data Client supports data transfer operations to and from the XNAT server. Check for the latest version of the XNAT Data Client at https://bintray.com/nrgxnat/generic/xnat-data-client/_latestVersion.")
@Getter
@Setter
@Accessors(prefix = "_")
@Slf4j
public class XnatDataClient {
    public static void main(String[] args) throws Exception {
        if (args.length > 0) {
            log.debug("Starting instance of XDC with the parameters: {}", StringUtils.join(args, ", "));
        } else {
            log.debug("Starting instance of XDC with no parameters");
        }
        XnatDataClient xdc = null;
        try {
            xdc = new XnatDataClient(args);
            if (xdc.getBean().getShouldContinue()) {
                final int status = xdc.launch();
                if (status != 0) {
                    System.exit(status);
                }
            }
        } catch (PintoException exception) {
            // Only warn on non-syntax format errors
            if (exception.getType() != PintoExceptionType.SyntaxFormat) {
                log.warn("An error occurred", exception);
            }
            if (xdc != null) {
                xdc.getBean().displayHelp();
            }
        }
    }

    public XnatDataClient(final String args) throws PintoException {
        this(args.split("\\s+"));
    }

    public XnatDataClient(final String[] args) throws PintoException {
        _properties = Reflection.getPropertiesForClass(getClass());
        if (_properties == null || _properties.stringPropertyNames().size() == 0) {
            throw new PintoException(PintoExceptionType.Configuration, "Couldn't load properties bundle for class: " + getClass().getName());
        }
        _bean = new XnatDataClientPintoBean(this, checkForOsaOverride(args));
        _printStream = _bean.getPrintStream();

        _clientBuilder = HttpClientBuilder.create();
        _context = HttpClientContext.create();
        _cookieStore = new BasicCookieStore();
    }

    public int launch() throws Exception {
        log.info("Launching XnatDataClient...");

        if (getBean().getRemote() == null) {
            throw new Exception("You must specify a remote URL until XDC supports file-system mapping.");
        }

        final HttpRequestBase request = HttpMethod.getHttpRequestObject(getBean().getMethod());
        request.setURI(buildUri());

        if (getBean().getLocal() != null) {
            setEntity(request);
        }

        final RequestConfig.Builder builder = RequestConfig.custom();
        final HttpHost              proxy   = configureProxy();
        if (proxy != null) {
            builder.setProxy(proxy);
        }
        request.setConfig(builder.build());
        configureAuthentication();

        if (getBean().hasHeaders()) {
            for (final String header : getBean().getHeaders().keySet()) {
                request.setHeader(header, getBean().getHeaders().get(header));
            }
        }

        _clientBuilder.setDefaultCookieStore(_cookieStore);
        if (getBean().getAcceptSelfSigned()) {
            _clientBuilder.setSSLSocketFactory(getPermissiveSocketFactory(true));
        } else if (getBean().getIgnoreCertErrors()) {
            _clientBuilder.setSSLSocketFactory(getPermissiveSocketFactory(false));
        }

        try (final CloseableHttpClient client = _clientBuilder.build();
             final InputStream stream = getDataStream()) {
            final boolean hasData = getBean().getData() != null;
            if (stream != null) {
                ((HttpEntityEnclosingRequest) request).setEntity(new InputStreamEntity(stream));
            } else if (hasData && !isPlainText()) {
                ((HttpEntityEnclosingRequest) request).setEntity(new UrlEncodedFormEntity(URLEncodedUtils.parse(getBean().getData(), StandardCharsets.UTF_8)));
            }
            try (final CloseableHttpResponse response = _context == null ? client.execute(request) : client.execute(request, _context)) {
                handleEntity(response, client);

                if (getBean().getDumpHeaders() != null) {
                    try (final PrintWriter writer = new PrintWriter(new FileWriter(getBean().getDumpHeaders()))) {
                        for (final Header header : response.getAllHeaders()) {
                            writer.println(header.getName() + ": " + header.getValue());
                            for (final HeaderElement element : header.getElements()) {
                                writer.println(element.getName() + ": " + element.getValue());
                            }
                        }
                    }
                }

                int status = response.getStatusLine().getStatusCode();

                log.info("Status: {}", response.getStatusLine());
                if (getBean().getShowStatusLine()) {
                    _printStream.println("HTTP status: " + response.getStatusLine());
                } else if (getBean().getShowStatusCode()) {
                    _printStream.println("HTTP status: " + status);
                }

                return status < 400 ? 0 : status;
            }

        }
    }

    private InputStream getDataStream() throws Exception {
        final boolean hasData       = getBean().getData() != null;
        final boolean hasBinaryData = getBean().getDataBinary() != null;
        if (!hasData && !hasBinaryData) {
            return null;
        }
        final boolean isPlainText = isPlainText();
        if (isPlainText && hasBinaryData) {
            throw new Exception("You've specified content type text/plain but are passing binary data.");
        }
        if (isPlainText || hasBinaryData) {
            final String data = isPlainText ? getBean().getData() : getBean().getDataBinary();
            if (data.startsWith("@")) {
                final File file = Paths.get(getBean().getData().substring(1)).toFile();
                if (!(file.exists() && file.isFile())) {
                    throw new Exception("The specified file " + file.getPath() + " does not exist or is not accessible.");
                }
                return Files.asByteSource(file).openBufferedStream();
            } else {
                return new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
            }
        }
        return null;
    }

    private boolean isPlainText() {
        return getBean().hasHeaders() && getBean().getHeaders().containsKey("Content-Type") && getBean().getHeaders().get("Content-Type").equals("text/plain");
    }

    private LayeredConnectionSocketFactory getPermissiveSocketFactory(final boolean selfSignedCompatible) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        return new SSLConnectionSocketFactory(new SSLContextBuilder().loadTrustMaterial(null, selfSignedCompatible ? new TrustSelfSignedStrategy() : new AllowAllCertsStrategy()).build());
    }

    private URI buildUri() throws URISyntaxException {
        final URIBuilder builder = new URIBuilder(getBean().getRemote().toString());
        if (getBean().getUseAbsolutePath()) {
            builder.addParameter("locator", "absolutePath");
        }
        if (getBean().getData() != null) {
            builder.addParameters(URLEncodedUtils.parse(getBean().getData(), StandardCharsets.US_ASCII));
        }
        if (getBean().getDataBinary() != null) {
            builder.addParameters(URLEncodedUtils.parse(getBean().getDataBinary(), Charset.defaultCharset()));
        }
        return builder.build();
    }

    private void setEntity(HttpRequestBase request) {
        File local = getBean().getLocal();
        if (local != null) {
            if (request instanceof HttpEntityEnclosingRequest) {
                FileEntity entity = new FileEntity(local, ContentType.create(getMimeType(local)));
                ((HttpEntityEnclosingRequest) request).setEntity(entity);
            }
        }
    }

    private Properties getProperties() {
        return _properties;
    }

    private PrintStream getPrintStream() {
        return _printStream;
    }

    private String[] checkForOsaOverride(final String[] args) {
        // If the args don't contain an display stream adapter override...
        if (!(ArrayUtils.contains(args, "-osa") || ArrayUtils.contains(args, "--outputStreamAdapter"))) {
            // Check to see if one is configured in the application properties.
            if (getProperties().contains("output.stream.adapter")) {
                return (String[]) ArrayUtils.add(ArrayUtils.add(args, "-osa"), getProperties().get("output.stream.adapter"));
            }
        }
        return args;
    }

    private String getMimeType(File file) {
        return MIME_TYPE_MAP.getContentType(file);
    }

    private void handleEntity(final HttpResponse response, final HttpClient client) throws IOException, URISyntaxException {
        final HttpEntity entity = response.getEntity();

        if (entity == null) {
            log.debug("I tried to handle the response entity, but the entity was null.");
            return;
        }

        try {
            final long size = entity.getContentLength();
            if (size == 0) {
                log.debug("Entity returned, but size was 0, so just finishing the entity handling.");
                return;
            }

            final Header contentType = entity.getContentType();
            final String value       = contentType != null ? contentType.getValue() : "";
            if (!StringUtils.isBlank(value) && value.equalsIgnoreCase("multipart/form-data")) {
                throw new RuntimeException("XDC does not currently handle multipart form data.");
            }

            log.debug("Found an entity: {} {} bytes in length", StringUtils.isBlank(value) ? "No content type found" : value, entity.getContentLength());

            // If this is a batch processing run...
            if (getBean().getBatch()) {
                if (!"application/json".equalsIgnoreCase(value)) {
                    throw new RuntimeException("XDC can currently only perform batch transfers with JSON inputs.");
                }
                handleBatchTransfer(client, entity);
            } else {
                handleSingleTransfer(entity);
            }
        } finally {
            EntityUtils.consume(entity);
        }
    }

    /**
     * This takes an entity that contains JSON code and pulls URIs (if {@link XnatDataClientPintoBean#getUseAbsolutePath()}
     * is set to <b>false</b>) and absolute paths (if {@link XnatDataClientPintoBean#getUseAbsolutePath()} is set to
     * <b>true</b>) and downloads or copies the resulting files.
     *
     * @param client The HTTP client instance to use
     * @param entity The entity returned by a response to a REST call that contains JSON with URIs or paths.
     */
    private void handleBatchTransfer(final HttpClient client, final HttpEntity entity) throws IOException, URISyntaxException {
        final String              targetColumn = getBean().getUseAbsolutePath() ? "absolutePath" : "URI";
        final Map<String, String> items        = new HashMap<>();
        try {
            final Iterator<JsonNode> nodes = MAPPER.readTree(entity.getContent()).path("ResultSet").path("Result").elements();
            while (nodes.hasNext()) {
                final JsonNode node   = nodes.next();
                final String   name   = ObjectUtils.defaultIfNull(node.get("Name"), node.get("ID")).asText();
                final String   target = node.get(targetColumn).asText();
                items.put(name, target);
                addPath(name, target);
            }
        } finally {
            EntityUtils.consume(entity);
        }

        final String commonPath = getCommonPath();
        for (final Map.Entry<String, String> item : items.entrySet()) {
            if (getBean().getListUris()) {
                _printStream.println(item.getValue());
            } else {
                // Here the name is the name of the resource file, the path is the full path to the item, and partial is
                // the difference between the common path and the full path to the item. This is so that paths within
                // archive structures can be constructed at the destination.
                final String name    = item.getKey();
                final String path    = item.getValue();
                final String partial = StringUtils.reverse(StringUtils.difference(StringUtils.reverse(name), StringUtils.reverse(StringUtils.difference(commonPath, path))));
                transfer(client, name, path, partial);
            }
        }
    }

    private void addPath(final String name, final String fullPath) {
        final String path = StringUtils.removeEnd(fullPath, name);
        if (_commonPath == null) {
            _commonPath = path;
        } else {
            trimPath(path);
        }
    }

    private void trimPath(final String path) {
        if (path.equals(_commonPath)) {
            return;
        }
        final int pathLength       = path.length();
        final int commonPathLength = _commonPath.length();
        if (pathLength > commonPathLength) {
            if (path.startsWith(_commonPath)) {
                return;
            }
        } else if (commonPathLength > pathLength) {
            if (_commonPath.startsWith(path)) {
                _commonPath = path;
                return;
            }
        }
        final char[] pathChars     = _commonPath.toCharArray();
        final char[] shorter       = pathLength < commonPathLength ? path.toCharArray() : pathChars;
        final char[] longer        = pathLength >= commonPathLength ? path.toCharArray() : pathChars;
        int          index         = 0;
        int          lastSeparator = 0;
        while (shorter[index] == longer[index]) {
            if (shorter[index] == '/') {
                lastSeparator = index;
            }
            index++;
        }
        _commonPath = new String(Arrays.copyOf(shorter, lastSeparator + 1));
    }

    private void transfer(final HttpClient client, final String name, final String target, final String partial) throws URISyntaxException, IOException {
        final File full = getRelativePathToOutput(partial);
        if (!full.exists()) {
            if (!full.mkdirs()) {
                throw new RuntimeException("Failed to create the directory: " + full.getAbsolutePath() + ". Please check permissions and accessibility to this path.");
            }
        }

        final HttpEntity instance;
        final File       incoming;
        final File       outgoing;
        if (getBean().getUseAbsolutePath()) {
            incoming = new File(getBean().hasPathPrefix() ? target.replace(getBean().getPathPrefixSource(), getBean().getPathPrefixDestination()) : target);
            instance = null;
        } else {
            incoming = null;
            final URI     remote  = getBean().getRemote();
            final HttpGet request = new HttpGet(new URI(remote.getScheme(), remote.getUserInfo(), remote.getHost(), remote.getPort(), target, null, null));
            instance = (_context == null ? client.execute(request) : client.execute(request, _context)).getEntity();
        }
        try (final InputStream input = incoming != null ? new FileInputStream(incoming) : instance.getContent()) {
            outgoing = new File(full, name);
            log.debug("Starting the transfer for {} to partial path {} from source {}", name, partial, target);
            if (getBean().getUseSymlinks()) {
                assert incoming != null;
                doSymlink(incoming, outgoing);
                display("Created symlink from " + incoming.getPath() + " to " + outgoing.getPath());
            } else {
                try (final OutputStream output = new FileOutputStream(outgoing)) {
                    notifyOnOutput(outgoing, doTransfer(input, output));
                }
            }
        } finally {
            if (instance != null) {
                EntityUtils.consume(instance);
            }
        }
    }

    private File getRelativePathToOutput(final String partial) {
        final File output;
        if (!StringUtils.isBlank(getBean().getOutputName())) {
            output = new File(getBean().getOutputName());
            if (output.exists()) {
                if (!output.isDirectory()) {
                    throw new RuntimeException("You must specify your output to be a directory to use this function.");
                }
            } else {
                log.debug("{} output folder: {}", output.mkdirs() ? "Created" : "Found", output.getAbsolutePath());
            }
        } else {
            output = new File(".");
            log.debug("Using working directory for output folder: {}", output.getAbsolutePath());
        }

        try {
            return new File(output.getCanonicalFile(), partial);
        } catch (IOException e) {
            throw new RuntimeException("Encountered error trying to resolve canonical file to: " + output.getAbsolutePath(), e);
        }
    }

    /**
     * Super simple abstraction around _printStream.println to allow for easy migration to more flexible output method
     * later. All display messages are also written out as debug logging messages.
     *
     * @param output The text to be displayed.
     */
    private void display(String output) {
        _printStream.println(output);
        log.debug(output);
    }

    private void doSymlink(final File incoming, final File outgoing) {
        try {
            Runtime.getRuntime().exec("ln -s " + incoming.getAbsolutePath() + " " + outgoing.getAbsolutePath());
        } catch (IOException exception) {
            log.error("Error trying to create symlinks", exception);
        }
    }

    private void handleSingleTransfer(final HttpEntity entity) throws IOException {
        if (entity == null) {
            throw new RuntimeException("There is no HTTP entity associated with the attempted transfer. Please check your URL and permissions at the server.");
        }

        // Do the single transfer operation.
        // If we have a specified output name, we'll use that regardless of whether this is text or not.

        // Figure out the appropriate display location.
        OutputStream output = null;
        try (final InputStream input = entity.getContent()) {
            final File   local;
            final String contentType = entity.getContentType() != null ? entity.getContentType().getValue() : "";
            if (!StringUtils.isBlank(getBean().getOutputName())) {
                local = new File(getBean().getOutputName());
                output = new FileOutputStream(resolveTarget(local, getSuggestedExtension(contentType)));
            } else if (!isTextContentType(contentType)) {
                // TODO: It'd be nice to re-use the downloaded file name if possible.
                local = generateUniqueFile(getSuggestedExtension(contentType));
                output = new FileOutputStream(local);
            } else {
                // This is a text file with no local, so we'll write out to the print stream.
                local = null;
                output = getPrintStream();
            }

            long totalBytesWritten = doTransfer(input, output);
            if (output instanceof PrintStream) {
                ((PrintStream) output).print('\n');
            }
            notifyOnOutput(local, totalBytesWritten);
        } finally {
            if (output != null) {
                output.flush();
                // Only close this if it's a file, otherwise it's standard out.
                if (output instanceof FileOutputStream) {
                    output.close();
                }
            }
        }
    }

    private void notifyOnOutput(final File local, final long totalBytesWritten) {
        if (totalBytesWritten > 0) {
            log.info("Wrote {} bytes to {}", totalBytesWritten, local != null ? local.getPath() : "output stream");
        } else {
            if (local != null) {
                if (!local.delete()) {
                    display("No contents found in response body, unable to delete indicated output file: " + local.getPath());
                } else {
                    display("No contents found in response body, no output file generated");
                }
            } else {
                display("No contents found in response body, no output file generated");
            }
        }
    }

    private File resolveTarget(final File local, final String suggestedExtension) {
        final File target;
        if (local.exists() && local.isDirectory()) {
            // TODO: It'd be nice to re-use the downloaded file name if possible.
            target = generateUniqueFile(local, suggestedExtension);
        } else if (!hasExtension(local)) {
            target = new File(local.getAbsolutePath() + "." + suggestedExtension);
        } else {
            target = local;
            if (!getBean().getOverwrite() && target.exists()) {
                getPrintStream().print("The file " + target.getName() + " already exists. Do you want to overwrite it? ");
                String confirm = null;
                try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
                    confirm = reader.readLine();
                } catch (IOException ioe) {
                    System.err.println("IO error reading input.");
                    System.exit(-1);
                }
                if (confirm == null || !(confirm.equalsIgnoreCase("y") || confirm.equalsIgnoreCase("yes"))) {
                    getPrintStream().println("Operation aborted.");
                    System.exit(0);
                }
            }
        }
        return target;
    }

    /**
     * This tells you whether the file has an extension that is separated by the '.' character and consists of one or
     * more alphanumeric characters.
     *
     * @param file The file with the name to be tested.
     *
     * @return True if the file name has an extension, false otherwise.
     */
    private boolean hasExtension(final File file) {
        final String name = file.getName();
        if (!name.contains(".")) {
            return false;
        }
        final String lastElement = name.substring(name.lastIndexOf("."));
        return !StringUtils.isBlank(lastElement) && lastElement.matches("^\\.[A-z0-9]+");
    }

    /**
     * Transfers content from the content input stream to the output output stream.
     *
     * @param content The stream from which to read.
     * @param output  The stream to which to write.
     *
     * @return The total number of bytes written during the transfer.
     *
     * @throws IOException When an error occurs during the transfer.
     */
    private long doTransfer(final InputStream content, final OutputStream output) throws IOException {
        if (content == null) {
            throw new RuntimeException("Tried to retrieve content, but got null stream in response.");
        }
        assert output != null : "Failed to initialize output stream.";
        final AtomicLong totalBytesWritten = new AtomicLong();
        try {
            final byte[] buffer = new byte[getBean().getBufferSize()];
            int          read;
            while ((read = content.read(buffer)) != -1) {
                output.write(buffer, 0, read);
                totalBytesWritten.addAndGet(read);
            }
        } finally {
            output.flush();
        }
        return totalBytesWritten.get();
    }

    /**
     * Creates a new uniquely named file in the current working folder.
     *
     * @param extension The extension to append to the file.
     *
     * @return A {@link File} object representing the new uniquely named file.
     */
    private File generateUniqueFile(final String extension) {
        return generateUniqueFile(null, extension);
    }

    /**
     * Creates a new uniquely named file in the working folder.
     *
     * @param folder    The folder in which to put the file.
     * @param extension The suggested extension to append to the file. This can be overridden if explicitly set.
     *
     * @return A {@link File} object representing the new uniquely named file.
     */
    private File generateUniqueFile(final File folder, final String extension) {
        int          index     = -1;
        File         local;
        final String pattern   = getFilenamePattern(extension);
        String       formatted = getGeneratedFileName(pattern, index);
        while ((local = folder == null ? new File(formatted) : new File(folder, formatted)).exists()) {
            index++;
            formatted = getGeneratedFileName(pattern, index);
        }
        return local;
    }

    private String getFilenamePattern(final String extension) {
        final String outputName = getBean().getOutputName();
        if (!StringUtils.isBlank(outputName)) {
            final String explicitExt  = FilenameUtils.getExtension(outputName);
            boolean      hasExtension = !StringUtils.isBlank(explicitExt);
            return (hasExtension ? FilenameUtils.removeExtension(outputName) : outputName) + "%s%s." + (hasExtension ? explicitExt : extension);
        } else {
            return String.format(GENERATED_FILE_NAME, "%s%s", extension);
        }
    }

    private String getGeneratedFileName(final String pattern, final int index) {
        return index == -1 ? String.format(pattern, "", "") : String.format(pattern, "-", StringUtils.leftPad(Integer.toString(index), 4, "0"));
    }

    private String getSuggestedExtension(final String contentType) {
        synchronized (EXTENSIONS_BY_MIME_TYPE) {
            if (EXTENSIONS_BY_MIME_TYPE.isEmpty()) {
                try (final BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/META-INF/mime.types")))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        if (!line.trim().startsWith("#")) {
                            final String[] atoms = line.trim().split("\\s+");
                            if (atoms.length > 1) {
                                EXTENSIONS_BY_MIME_TYPE.put(atoms[0], atoms[1]);
                            }
                        }
                    }
                } catch (IOException e) {
                    //
                }
            }
        }
        // If there's no content type specified, go with text since something will open that and you can figure it out from there.
        return StringUtils.defaultIfBlank(EXTENSIONS_BY_MIME_TYPE.get(contentType), EXTENSIONS_BY_MIME_TYPE.get("text/plain"));
    }

    private boolean isTextContentType(final String contentType) {
        return getTextContentTypes().contains(contentType.contains(";") ? contentType.substring(0, contentType.indexOf(';')) : contentType);
    }

    private List<String> getTextContentTypes() {
        synchronized (TEXT_CONTENT_TYPES) {
            if (TEXT_CONTENT_TYPES.size() == 0) {
                TEXT_CONTENT_TYPES.addAll(Arrays.asList(getProperties().getProperty("content.types.text").split("\\s*,\\s*")));
            }
        }
        return TEXT_CONTENT_TYPES;
    }

    private void configureAuthentication() {
        if (StringUtils.isNotBlank(getBean().getUsername())) {
            final URI uri        = getBean().getRemote();
            HttpHost  targetHost = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
            _credentialsProvider.setCredentials(new AuthScope(targetHost.getHostName(), targetHost.getPort()), new UsernamePasswordCredentials(getBean().getUsername(), getBean().getPassword()));

            // Create AuthCache instance
            AuthCache authCache = new BasicAuthCache();

            // Generate BASIC scheme object and add it to the local auth cache
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(targetHost, basicAuth);

            // Add AuthCache to the execution context
            _context.setCredentialsProvider(_credentialsProvider);
            _context.setAuthCache(authCache);
        } else if (StringUtils.isNotBlank(getBean().getSessionId())) {
            // Verify the JSESSIONID.
            for (Cookie cookie : _cookieStore.getCookies()) {
                if (cookie.getName().equals("JSESSIONID")) {
                    return;
                }
            }
            // If we don't have a JSESSIONID cookie, add it.
            final BasicClientCookie cookie = new BasicClientCookie("JSESSIONID", getBean().getSessionId());
            cookie.setDomain(getBean().getRemote().getHost());
            _cookieStore.addCookie(cookie);
        }
    }

    private HttpHost configureProxy() {
        if (getBean().getProxy() == null) {
            return null;
        }

        final HttpHost proxy = new HttpHost(getBean().getProxy().getHost(), getBean().getProxy().getPort(), getBean().getProxy().getScheme());
        if (StringUtils.isNotBlank(getBean().getProxy().getUserInfo())) {
            final String[] credentials = getBean().getProxy().getUserInfo().split(":");
            _credentialsProvider.setCredentials(new AuthScope(getBean().getProxy().getHost(), getBean().getProxy().getPort()), new UsernamePasswordCredentials(credentials[0], credentials[1]));
        }
        return proxy;
    }

    private static class AllowAllCertsStrategy implements TrustStrategy {
        @Override
        public boolean isTrusted(X509Certificate[] chain, String authType) {
            return true;
        }
    }

    private static final String               GENERATED_FILE_NAME     = "download%s.%s";
    private static final MimetypesFileTypeMap MIME_TYPE_MAP           = new MimetypesFileTypeMap();
    private static final List<String>         TEXT_CONTENT_TYPES      = new ArrayList<>();
    private static final Map<String, String>  EXTENSIONS_BY_MIME_TYPE = new HashMap<>();
    private static final String               PLAIN_TEXT_EXTENSION    = "txt";
    private static final ObjectMapper         MAPPER                  = new ObjectMapper();

    private final CredentialsProvider _credentialsProvider = new BasicCredentialsProvider();

    private final XnatDataClientPintoBean _bean;
    private final HttpClientBuilder       _clientBuilder;
    private final CookieStore             _cookieStore;
    private final HttpClientContext       _context;
    private final PrintStream             _printStream;
    private final Properties              _properties;

    private String _commonPath;
}
